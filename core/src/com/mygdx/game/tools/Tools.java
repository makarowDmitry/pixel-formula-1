package com.mygdx.game.tools;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.GameObject;
import com.mygdx.game.resources.Configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс содержащий в себе дополнительные методы, которые используются в нескольких классах
 *
 * @author Волбенко А.К. 17ИТ18
 */
public final class Tools implements Configuration {

    /**
     * Метод создающий Rectangle нажатия пальца
     *
     * @param x      - позиция  X
     * @param y      - позиция Y
     * @param xScale - масштам разрешения X
     * @param yScale - масштаб разрешения Y
     * @return - Rectangle нажатия пальца
     */
    public Rectangle userTouchRectangle(int x, int y, float xScale, float yScale, int resHeight) {
        return new Rectangle(x * xScale, resHeight - y * yScale, 3, 3);
    }

    /**
     * Изменение позиции outro
     *
     * @param transitionPos        - позиция перехода
     * @param transitionSizeWeight - размер ширины текстуры перехода
     * @param resolutionWidth      - ширина экрана
     * @param speed                - скорость перехода
     * @return - позиция перехода
     */
    public int setTransitionPos(int transitionPos, int transitionSizeWeight, int resolutionWidth, int speed) {
        if (transitionPos > (transitionSizeWeight - resolutionWidth) * -1)
            return transitionPos - speed;
        else return transitionPos;
    }

    /**
     * Метод изменяющий размер взависимости от изначального разрешения и будующего
     *
     * @param number             - число для изменения
     * @param originalScreenSize - оригинальное разрешине
     * @param newScreenSize      - новое разрешение
     * @return - измененное число
     */
    public int resizeNumberByResolution(int number, int originalScreenSize, int newScreenSize) {
        return (int) ((float) newScreenSize / originalScreenSize * number);
    }

    /**
     * Проверка на столкновении
     *
     * @param mainObject   - основной объект, для которого идет проверка
     * @param otherObjects - объекты с которыми проверяется столкновение
     * @return - boolean столкновения
     */
    public boolean checkTouch(Rectangle mainObject, ArrayList<GameObject> otherObjects) {
        for (GameObject otherObj : otherObjects) {
            if (mainObject.overlaps(otherObj.getHitBox())) {
                return true;
            }
        }
        return false;
    }

    public boolean checkTouch(Rectangle mainObject, HashMap<GameObject, Integer> otherObjects) {
        for (Map.Entry<GameObject, Integer> otherObj : otherObjects.entrySet()) {
            if (mainObject.overlaps(otherObj.getKey().getHitBox())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Создание рандомного числа с учетом размера
     *
     * @return - позиция по x
     */
    public int getRandomNumber(int leftLimit, int rightLimit, int objWidth) {
        return MathUtils.random(leftLimit, rightLimit - objWidth);
    }
}
