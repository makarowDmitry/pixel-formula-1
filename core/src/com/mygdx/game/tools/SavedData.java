package com.mygdx.game.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Класс отвечающий за сохранение данных
 *
 * @author Макаров Д.А
 */
public class SavedData {

    public Preferences pref = Gdx.app.getPreferences("My Preferences");

    /**
     * Добавление новых целочисленных данных в Preferences
     * @param key - ключ для обращения к данным
     * @param data - данные
     */
    public void addIntPreferences(String key, int data){
        pref.putInteger(key, data);
        pref.flush();
    }

}
