package com.mygdx.game.tools;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.resources.Configuration;

/**
 * Класс Control реализует управление машиной с помощью клавиатуры или сенсора
 *
 * @author Волбенко А.К 17ИТ18
 */
public class Control implements Configuration {
    private int speedX = 0;
    private int speedY = 0;

    private int touchPositionX = 0;
    private int touchPositionY = 0;

    private boolean touch = false;

    public int getSpeedX() {
        return speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public int getTouchPositionX() {
        return touchPositionX;
    }

    public int getTouchPositionY() {
        return touchPositionY;
    }

    public boolean getTouch() {
        return touch;
    }

    /**
     * Запуск методов управления
     */
    public void main() {
        stickCreate();
        stickWork(touch, STICK_RADIUS, STICK_DEAD_ZONE, STICK_SCALE_STEP);
    }

    /**
     * Инициализация стика и запоминание первого касания
     */
    private void stickCreate() {
        if (Gdx.input.isTouched() && !touch) {
            touchPositionX = (int) (Gdx.input.getX() * X_SCALE_MAIN);
            touchPositionY = RESOLUTION_HEIGHT_MAIN - (int) (Gdx.input.getY() * Y_SCALE_MAIN);
            touch = true;
        } else if (!Gdx.input.isTouched()) {
            touch = false;
        }
    }

    /**
     * Вычисление скорости движения машины от разница первого касания и дальнейшего положения пальца
     *
     * @param touch     - проверка касания
     * @param stickR    - радиус
     * @param deadZone  - мертвая зона
     * @param scaleStep - масштаб шага
     */
    private void stickWork(boolean touch, int stickR, int deadZone, int scaleStep) {
        if (touch) {
            //Разница между положением стика и положением пальца
            int stickX = (int) (Gdx.input.getX() * X_SCALE_MAIN) - touchPositionX;
            int stickY = (int) (RESOLUTION_HEIGHT_MAIN - (Gdx.input.getY() * Y_SCALE_MAIN)) - touchPositionY;

            //Ограничение стика
            if (stickX > stickR) stickX = stickR;
            if (stickX < (stickR * -1)) stickX = stickR * -1;
            if (stickY > stickR) stickY = stickR;
            if (stickY < (stickR * -1)) stickY = stickR * -1;

            //Расчет скорости от положения стика
            if (stickX > deadZone)
                speedX = (int) Math.ceil((double) (stickX - deadZone) / scaleStep);
            else if (stickX < deadZone * -1)
                speedX = (int) Math.ceil((double) (stickX + (deadZone * -1)) / scaleStep);
            else speedX = 0;
            if (stickY > deadZone)
                speedY = (int) Math.ceil((double) (stickY - deadZone) / scaleStep);
            else if (stickY < deadZone * -1)
                speedY = (int) Math.ceil((double) (stickY + (deadZone * -1)) / scaleStep);
            else speedY = 0;
        }
        if (!touch) {
            //Приравнивание скоростей к нулю, если нет касания
            speedX = 0;
            speedY = 0;
        }
    }

    /**
     * Метод отвечающий за переменную анимации поворота машины
     *
     * @param x          - положение машины
     * @param leftSideX  - положение левого ограничения
     * @param rightSideX - положение правого ограничения
     * @param carWight   - ширина машины
     * @return - градус поворота машины
     */
    public int getTurnNumber(int x, int leftSideX, int rightSideX, int carWight) {
        if (x + carWight >= rightSideX) return 0;
        else if (x <= leftSideX) return 0;
        else return speedX * -1;
    }

    /**
     * Вычисление числа для скорости анимации, взависимости от передвижения по вертикали
     */
    public float getSpeedAnimation() {
        return 1f + ((float) speedY / 10);
    }

    /**
     * Осуществление ограничения передвижения по горизонтали
     *
     * @param x          - положение игрока по горизонтали
     * @param leftSideX  - положение левого ограничения
     * @param rightSideX - положение правого ограничения
     * @param carWight   - ширина машины
     * @return - отфильтрованное положение машины игрока по горизонтали
     */
    public int playerXPosLimitation(int x, int leftSideX, int rightSideX, int carWight) {
        if (x + carWight >= rightSideX) return rightSideX - carWight;
        else return Math.max(x, leftSideX);
    }

    /**
     * Осуществление ограничения передвижения по вертикали
     *
     * @param y         - полодение игрока по вертикали
     * @param topSideY  - положение верхнего ограничения
     * @param downSideY - положение нижниго ограничения
     * @param carLength - длина машины
     * @return - отфильтрованное положение машины игрока по вертикали
     */
    public int playerYPosLimitation(int y, int topSideY, int downSideY, int carLength) {
        if (y + carLength >= topSideY) return topSideY - carLength;
        else return Math.max(y, downSideY);
    }
}
