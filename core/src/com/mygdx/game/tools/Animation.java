package com.mygdx.game.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Класс Animation реализует анимацию, путем смены картинок через определенное время
 *
 * @author Волбенко А.К 17ИТ18
 */
public class Animation {

    private Array<TextureRegion> frames;
    private float maxFrameTime;
    private float currentFrameTime;
    private int frameCount;
    private int frame;

    /**
     * Метод Animation режет текстуру на кадры и задает длительность 1 кадра
     *
     * @param texture    - текстура с кадрами для анимации
     * @param frameCount - количество кадров в текстуре
     * @param cycleTime  - длительность анимации
     */
    public Animation(Texture texture, int frameCount, float cycleTime) {
        frames = new Array<>();
        TextureRegion region = new TextureRegion(texture);
        int frameWidth = region.getRegionWidth() / frameCount;
        for (int i = 0; i < frameCount; i++) {
            frames.add(new TextureRegion(region, i * frameWidth, 0, frameWidth, region.getRegionHeight()));
        }
        this.frameCount = frameCount;
        maxFrameTime = cycleTime / frameCount;
        frame = 0;
    }

    /**
     * Метод отвечающий за смену кадров анимации
     *
     * @param dt - время работы одного тика метода render
     */
    public void update(float dt, boolean cycle) {
        currentFrameTime += dt;
        if (currentFrameTime >= maxFrameTime) {
            frame++;
            currentFrameTime = 0;
        }
        if (frame >= frames.size && cycle)
            frame = 0;
        if (frame >= frames.size && !cycle)
            frame--;
    }

    /**
     * getter кадра
     *
     * @return - текстура кадра
     */
    public TextureRegion getFrame() {
        return frames.get(frame);
    }

    /**
     * Сброс анимации до первого кадра
     */
    public void resetAnimation(){
        frame = 0;
    }
}
