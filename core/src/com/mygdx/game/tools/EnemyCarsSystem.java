package com.mygdx.game.tools;

import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.GameObject;
import com.mygdx.game.resources.Configuration;
import com.mygdx.game.resources.Resources;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс отвечающий за спавн вражеских машины и проверку столкновений с ними
 *
 * @author Волбенко А.К 17ИТ18
 */
public class EnemyCarsSystem implements Configuration, Resources {

    private float timer;
    private float startMoveTime = 0;
    private float dif = 1;
    private HashMap<GameObject, Integer> enemyCars = new HashMap<>();

    /**
     * Запуск основных методом для работы
     *
     * @param dt - delta time
     */
    public void main(float dt, float dif) {
        this.dif = dif;
        startMoveTimer(dt, startMoveTime);
        moveObject();
        updateAnimation(dt * 0.8f);
        enemyCars = deleteObject(enemyCars);
    }

    private void updateAnimation(float dt) {
        for (Map.Entry<GameObject, Integer> object : enemyCars.entrySet()) {
            object.getKey().updateAnimation(dt, true);
        }
    }

    private void moveObject() {
        for (Map.Entry<GameObject, Integer> object : enemyCars.entrySet()) {
            object.getKey().setPos(object.getKey().getX(), object.getKey().getY() - object.getValue());
        }
    }

    private HashMap<GameObject, Integer> deleteObject(HashMap<GameObject, Integer> objectsMap) {
        HashMap<GameObject, Integer> resultMap = new HashMap<>();
        for (Map.Entry<GameObject, Integer> object : objectsMap.entrySet()) {
            if (object.getKey().getY() >= -object.getKey().getTexture().getRegionHeight())
                resultMap.put(object.getKey(), object.getValue());
        }
        return resultMap;
    }

    public HashMap<GameObject, Integer> getEnemyCars() {
        return enemyCars;
    }

    public void setEnemyCars(HashMap<GameObject, Integer> enemyCars) {
        this.enemyCars = enemyCars;
    }

    /**
     * Таймер запуска машины
     *
     * @param dt        - delta time
     * @param spawnTime - время запуска
     */
    private void startMoveTimer(float dt, float spawnTime) {
        timer += dt;
        if (timer >= spawnTime) {
            timer = 0;
            setStartMoveTime();
            createEnemyCar();
        }
    }

    /**
     * Создание рандомного времени спавна
     */
    private void setStartMoveTime() {
        startMoveTime = MathUtils.random(ENEMY_START_TIME_BOT_LIMIT, ENEMY_SPEED_TOP_LIMIT);
    }

    /**
     * Добавление машины и скорости в массивы
     */
    private void createEnemyCar() {
        enemyCars.put(createCarObject(getXPos()), createRandomSpeed()
        );
    }

    /**
     * Метод создающий позицию спавна машины
     * Проверяет позицию относительно уже имеющихся машин
     * Если позиция новой машины входит в одну из существующих, то позиция меняется
     *
     * @return - позицию новой машины
     */
    private int getXPos() {
        Tools tools = new Tools();
        boolean checkOverlaps = true;
        int posX = tools.getRandomNumber(LIMIT_LEFT_X_POS, LIMIT_RIGHT_X_POS, CAR_WIDTH);
        if (enemyCars.size() != 0) {
            while (checkOverlaps) {
                for (Map.Entry<GameObject, Integer> car : enemyCars.entrySet()) {
                    if (posX >= car.getKey().getX() && posX <= car.getKey().getX() + CAR_WIDTH ||
                            posX + CAR_WIDTH >= car.getKey().getX() && posX + CAR_WIDTH <= car.getKey().getX() + CAR_WIDTH) {
                        posX = tools.getRandomNumber(LIMIT_LEFT_X_POS, LIMIT_RIGHT_X_POS, CAR_WIDTH);
                        checkOverlaps = true;
                        break;
                    } else {
                        checkOverlaps = false;
                    }
                }
            }
        }
        return posX;
    }

    /**
     * Создание объекта машины
     *
     * @param x - позиция по x
     * @return - готовый объект машины
     */
    private GameObject createCarObject(int x) {
        return new GameObject(CARS_TEXTURES[MathUtils.random(0, CARS_TEXTURES.length - 1)],
                x, RESOLUTION_HEIGHT_MAIN,
                HIT_BOX_CAR_WIDTH, HIT_BOX_CAR_HEIGHT,
                HIT_BOX_CAR_X_POS_SHIFT, 0,
                ANIM_CAR_FRAME_COUNT, ANIM_CAR_CYCLE_TIME);
    }

    /**
     * Создание рандомной скорости передвижения
     *
     * @return - скорость движения
     */
    private int createRandomSpeed() {
        return (int) (MathUtils.random(ENEMY_SPEED_BOT_LIMIT, ENEMY_START_TIME_TOP_LIMIT) * dif);
    }
}
