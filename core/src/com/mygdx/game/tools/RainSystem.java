package com.mygdx.game.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.GameObject;
import com.mygdx.game.resources.Configuration;
import com.mygdx.game.resources.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RainSystem implements Resources, Configuration {

    private final Tools tools;

    public RainSystem() {
        tools = new Tools();
        setRainSpawn();
    }

    //Статус дождя
    private boolean rainLive = false;

    /**
     * Запуск методов, которые завязаны на времени
     *
     * @param dt - delta time
     */
    public void update(float dt, float dif) {
        rainTimers(dt);
        rainAnimationController(dt);
        puddlesController(dt, dif);
    }

    //Таймеры спавна и длительности дождя
    private float rainSpawn = 0;
    private float rainDuration = 0;
    private float rainDurationTimer = 0;

    /**
     * Метод таймеров жизни и спавна дождя
     *
     * @param dt - delta time
     */
    private void rainTimers(float dt) {
        if (rainSpawn > 0 && !rainLive)
            rainSpawn -= dt;
        else if (!rainLive && rainSpawn <= 0) {
            rainLive = true;
            setRainDuration();
            setRainSpawn();
            resetRainAnimations();
        }

        if (rainDurationTimer > 0 && rainLive)
            rainDurationTimer -= dt;
        else if (rainLive && rainDurationTimer <= 0) {
            rainLive = false;
            setRainDuration();
            setRainSpawn();
        }
    }

    /**
     * Установка таймера спавна дождя
     */
    private void setRainSpawn() {
        rainSpawn = MathUtils.random(RAIN_SPAWN_BOTTOM_LIMIT, RAIN_SPAWN_TOP_LIMIT);
    }

    /**
     * Установка таймера длительности дождя
     */
    private void setRainDuration() {
        rainDuration = MathUtils.random(RAIN_DURATION_BOTTOM_LIMIT, RAIN_DURATION_TOP_LIMIT);
        rainDurationTimer = rainDuration;
    }

    //Анимации дождя
    private final Animation rainStart = new Animation(
            RAIN_START,
            ANIM_RAIN_START_FRAME_COUNT,
            ANIM_RAIN_START_CYCLE_TIME
    );
    private final Animation rainStatic = new Animation(
            RAIN_STATIC,
            ANIM_RAIN_STATIC_FRAME_COUNT,
            ANIM_RAIN_STATIC_CYCLE_TIME
    );
    private final Animation rainEnd = new Animation(
            RAIN_END,
            ANIM_RAIN_END_FRAME_COUNT,
            ANIM_RAIN_END_CYCLE_TIME
    );

    //Текстура дождя для рисовки
    private TextureRegion rainFrame;

    /**
     * Обновление анимаций
     * Установка кадра нужной анимации в зависимости от времени
     *
     * @param dt - delta time
     */
    private void rainAnimationController(float dt) {
        if (rainDurationTimer >= rainDuration - ANIM_RAIN_START_CYCLE_TIME && rainLive) {
            rainStart.update(dt, false);
            rainFrame = rainStart.getFrame();
        } else if (rainDurationTimer > ANIM_RAIN_END_CYCLE_TIME && rainLive) {
            rainStatic.update(dt, true);
            rainFrame = rainStatic.getFrame();
        } else if (rainDurationTimer <= ANIM_RAIN_END_CYCLE_TIME && rainLive) {
            rainEnd.update(dt, false);
            rainFrame = rainEnd.getFrame();
        }
    }

    /**
     * Установка анимаци дождя на первый кадр
     */
    private void resetRainAnimations() {
        rainStart.resetAnimation();
        rainStatic.resetAnimation();
        rainEnd.resetAnimation();
    }

    public TextureRegion getRainFrame() {
        return rainFrame;
    }

    public boolean getRainLive() {
        return rainLive;
    }

    //Массив луж
    private final ArrayList<GameObject> puddles = new ArrayList<>();
    //Таймер спавна луж
    private float puddlesTimer;

    /**
     * Контроллер луж
     * - Отсчет таймера спавна лужи
     * - Создание лужи и установка нового значения таймера при его окончании
     * - Удаление луж, которые находятся за экраном и больше не будут нужны для игры
     * - Движение луж вместе с трассой
     * - Обновление анимаций луж
     *
     * @param dt - delta time
     */
    private void puddlesController(float dt, float dif) {
        if (rainLive)
            puddlesTimer -= dt;

        if (puddlesTimer <= 0 && rainLive) {
            setPuddlesTimer();
            createPuddle();
        }

        delPuddles();
        movePuddles(dif);
        updatePuddlesAnimation(dt);
    }

    /**
     * Установка таймера спавна следующей лужи
     */
    private void setPuddlesTimer() {
        puddlesTimer = MathUtils.random(PUDDLES_SPAWN_BOT, PUDDLES_SPAWN_TOP);
    }

    /**
     * Удаление луж, которые находятся за экраном
     */
    private void delPuddles() {
        if (puddles.size() != 0)
            for (int i = 0; i < puddles.size(); i++) {
                if (puddles.get(i).getY() <= -PUDDLE_HEIGHT) {
                    puddles.remove(i);
                    i--;
                }
            }
    }

    /**
     * Обновление анимации луж
     *
     * @param dt - delta time
     */
    private void updatePuddlesAnimation(float dt) {
        if (puddles.size() != 0)
            for (GameObject puddle : puddles) {
                puddle.updateAnimation(dt, true);
            }
    }

    public ArrayList<GameObject> getPuddles() {
        return puddles;
    }

    /**
     * Создание лужи и добавление ее в массив
     */
    private void createPuddle() {
        Texture puddle = getRandomPuddleTexture();
        puddles.add(new GameObject(puddle,
                tools.getRandomNumber(LIMIT_LEFT_X_POS, LIMIT_RIGHT_X_POS, PUDDLE_WIDTH), RESOLUTION_HEIGHT_MAIN,
                puddle.getWidth(), puddle.getHeight(),
                0, 0,
                ANIM_PUDDLE_FRAME_COUNT, ANIM_PUDDLE_CYCLE_TIME)
        );
    }

    /**
     * Взять рандомную текстуру из массива
     *
     * @return - текстура лужи
     */
    private Texture getRandomPuddleTexture() {
        return PUDDLES_TEXTURES[MathUtils.random(0, PUDDLES_TEXTURES.length - 1)];
    }

    /**
     * Движение луж
     *
     * @param dif - сложность игры, от которой зависит скорость луж
     */
    private void movePuddles(float dif) {
        if (puddles.size() > 0) {
            for (GameObject object : puddles) {
                object.setPos(
                        object.getX(),
                        object.getY() - (int) (BACKGROUND_SPEED * dif)
                );
            }
        }
    }

    /**
     * Метод осуществляющее пермещение машины при попадании на лужу
     *
     * @param objects - коллекция объектов(из-за того, что enemyCars использует HashMap)
     * @return - коллекция с измененными положениями
     */
    public HashMap<GameObject, Integer> sliding(HashMap<GameObject, Integer> objects) {
        for (Map.Entry<GameObject, Integer> object : objects.entrySet()) {
            if (tools.checkTouch(object.getKey().getHitBox(), puddles))
                if (MathUtils.randomBoolean())
                    object.getKey().setPos(object.getKey().getX() + MathUtils.random(SLIDE_BOTTOM_LIMIT, SLIDE_TOP_LIMIT), object.getKey().getY());
                else
                    object.getKey().setPos(object.getKey().getX() - MathUtils.random(SLIDE_BOTTOM_LIMIT, SLIDE_TOP_LIMIT), object.getKey().getY());
        }
        return objects;
    }

    /**
     * Метод осуществляющее пермещение машины при попадании на лужу
     *
     * @param object - объект
     * @return - объект с измененным положением
     */
    public GameObject sliding(GameObject object) {
        if (tools.checkTouch(object.getHitBox(), puddles))
            if (MathUtils.randomBoolean())
                object.setPos(object.getX() + MathUtils.random(SLIDE_BOTTOM_LIMIT, SLIDE_TOP_LIMIT), object.getY() - 1);
            else
                object.setPos(object.getX() - MathUtils.random(SLIDE_BOTTOM_LIMIT, SLIDE_TOP_LIMIT), object.getY());
        return object;
    }
}