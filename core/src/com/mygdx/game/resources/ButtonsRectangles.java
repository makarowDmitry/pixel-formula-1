package com.mygdx.game.resources;

import com.badlogic.gdx.math.Rectangle;

/**
 * Интерфейс содержащий Rectangle кнопок
 *
 * @author Волбенко А.К 17ИТ18
 */
public interface ButtonsRectangles {
    //Кнопки интерфейса в игре
    Rectangle BUTTON_GAME_PAUSE = new Rectangle(345, 740, 50, 50);
    Rectangle BUTTON_GAME_RESUME = new Rectangle(180, 320, 50, 50);
    Rectangle BUTTON_GAME_RESTART = new Rectangle(230, 400, 50, 50);
    Rectangle BUTTON_GAME_EXIT = new Rectangle(130, 400, 50, 50);

    //Кнопки интерфейса в меню
    Rectangle BUTTON_MENU_START = new Rectangle(45, 385, 310, 100);
    Rectangle BUTTON_MENU_CHOICE = new Rectangle(45, 235, 310, 100);

    //Выбор машины
    Rectangle CHOICE_FIRST_CAR = new Rectangle(30, 335, 90, 160);
    Rectangle CHOICE_SECOND_CAR = new Rectangle(130, 335, 90, 160);
    Rectangle CHOICE_THIRD_CAR = new Rectangle(30, 170, 90, 160);
    Rectangle CHOICE_FOURTH_CAR = new Rectangle(130, 170, 90, 160);
    Rectangle CHOICE_FIFTH_CAR = new Rectangle(30, 5, 90, 160);
    Rectangle CHOICE_SIXTH_CAR = new Rectangle(130, 5, 90, 160);
}
