package com.mygdx.game.resources;

import com.badlogic.gdx.graphics.Texture;

import javax.xml.soap.Text;

/**
 * Интерфейс Resources содержит в себе все текстуры, нужные для отображения игры
 *
 * @author Макаров М.А. 17ИТ18
 */
public interface Resources {

    //Текстуры машин
    Texture[] CARS_TEXTURES = {
            new Texture("cars/RussianCar.png"),
            new Texture("cars/RedBullCar.png"),
            new Texture("cars/MclarenCar.png"),
            new Texture("cars/FerrariCar.png"),
            new Texture("cars/LotusCar.png"),
            new Texture("cars/RenaultCar.png")
    };

    //Текстуры класса Choice
    Texture[] CARS_CHOICE = {
            new Texture("choice/RussianCarChoice.png"),
            new Texture("choice/RedBullCarChoice.png"),
            new Texture("choice/MclarenCarChoice.png"),
            new Texture("choice/FerrariCarChoice.png"),
            new Texture("choice/LotusCarChoice.png"),
            new Texture("choice/RenaultCarChoice.png")
    };

    //Текстуры дождя
    Texture RAIN_START = new Texture("rain/rainStart.png");
    Texture RAIN_STATIC = new Texture("rain/rainMid.png");
    Texture RAIN_END = new Texture("rain/rainEnd.png");
    Texture[] PUDDLES_TEXTURES = {
            new Texture("rain/puddle.png")
    };

    //Текстуры класса MyGdxGame
    Texture BUTTON_PAUSE = new Texture("buttons/PauseButton.png");
    Texture BUTTON_RESUME = new Texture("buttons/ResumeButton.png");
    Texture BUTTON_EXIT = new Texture("buttons/ExitButton.png");
    Texture BUTTON_RESTART = new Texture("buttons/RestartButton.png");
    Texture BUTTON_START = new Texture("buttons/StartButton.png");
    Texture BUTTON_CHOICE = new Texture("buttons/ChoiceButton.png");

    //Текстуры задних фонов
    Texture BACKGROUND_MAIN = new Texture("background/Track.png");
    Texture BACKGROUND_FINISH = new Texture("background/FinishBackground.png");
    Texture BACKGROUND_FINISH_UP_LAYER = new Texture("background/FinishUpLayerObjects.png");
    Texture BACKGROUND_CHOICE = new Texture("choice/chooseBackground.png");
    Texture BACKGROUND_PAUSE = new Texture("background/PauseBackground.png");

    //Текстуры дополнительного интерфейса
    Texture DIFFICULTY_LAP_TEXTURE = new Texture("raceTrack.png");
    Texture MENU_BEST_LAP = new Texture("bestLap.png");
    Texture RECORD = new Texture("buttons/InDeveloping.png");
    Texture STICK_TEXTURE = new Texture("buttons/Stick.png");
    Texture INTRO = new Texture("Intro.png");
    Texture OUTRO = new Texture("Outro.png");

    //Тектсура дождя
}
