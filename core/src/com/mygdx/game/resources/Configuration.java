package com.mygdx.game.resources;

import com.badlogic.gdx.Gdx;

/**
 * Интерфейс Configuration содержит в себе переменные, для которых должна быть возможность изменения для отладки
 *
 * @author Волбенко А.К. 17ИТ18
 */
public interface Configuration {

    //Разрешения экрана
    int RESOLUTION_WIDTH_MAIN = 400;
    int RESOLUTION_HEIGHT_MAIN = 800;
    float X_SCALE_MAIN = (float) RESOLUTION_WIDTH_MAIN / Gdx.app.getGraphics().getWidth();
    float Y_SCALE_MAIN = (float) RESOLUTION_HEIGHT_MAIN / Gdx.app.getGraphics().getHeight();
    //Разрешение экрана выбора

    int RESOLUTION_WIDTH_CHOICE = 250;
    int RESOLUTION_HEIGHT_CHOICE = 500;
    float X_SCALE_CHOICE = (float) RESOLUTION_WIDTH_CHOICE / Gdx.app.getGraphics().getWidth();
    float Y_SCALE_CHOICE = (float) RESOLUTION_HEIGHT_CHOICE / Gdx.app.getGraphics().getHeight();

    //Интро
    int INTRO_OUTRO_SPEED = 50;

    //Размеры объектов
    int CAR_WIDTH = 60;
    int CAR_HEIGHT = 155;
    int PUDDLE_WIDTH = 20;
    int PUDDLE_HEIGHT = 20;

    //Настройки анимации
    int ANIM_CAR_FRAME_COUNT = 5;
    float ANIM_CAR_CYCLE_TIME = 0.5f;
    int ANIM_PUDDLE_FRAME_COUNT = 1;
    float ANIM_PUDDLE_CYCLE_TIME = 1;
    int ANIM_RAIN_START_FRAME_COUNT = 1;
    float ANIM_RAIN_START_CYCLE_TIME = 3;
    int ANIM_RAIN_STATIC_FRAME_COUNT = 1;
    float ANIM_RAIN_STATIC_CYCLE_TIME = 3;
    int ANIM_RAIN_END_FRAME_COUNT = 1;
    float ANIM_RAIN_END_CYCLE_TIME = 3;
    int BACKGROUND_SPEED = 7;

    //Размеры хитбокса объектов и сопутствующие данные
    int HIT_BOX_CAR_WIDTH = 54;
    int HIT_BOX_CAR_X_POS_SHIFT = (CAR_WIDTH - HIT_BOX_CAR_WIDTH) / 2;
    int HIT_BOX_CAR_HEIGHT = 150;

    //Координаты ограничения движения
    int LIMIT_LEFT_X_POS = 55;
    int LIMIT_RIGHT_X_POS = 345;
    int LIMIT_BOTTOM_Y_POS = 0;
    int LIMIT_TOP_Y_POS = 800;

    //Настройки стика управления
    int STICK_RADIUS = 100;
    int STICK_DEAD_ZONE = 10;
    int STICK_SCALE_STEP = 20;

    //Настройки сложности
    float LAP_TIME = 50f;
    float RAIN_SPAWN_BOTTOM_LIMIT = 0;
    float RAIN_SPAWN_TOP_LIMIT = 100;
    float RAIN_DURATION_BOTTOM_LIMIT = 7;
    float RAIN_DURATION_TOP_LIMIT = 35;
    float PUDDLES_SPAWN_BOT = 0;
    float PUDDLES_SPAWN_TOP = 1.3f;
    int SLIDE_BOTTOM_LIMIT = 1;
    int SLIDE_TOP_LIMIT = 2;

    //Стартовое положение игрока
    int PLAYER_X_POS = 170;
    int PLAYER_Y_POS = 10;

    //Настройка вражеских машин
    int ENEMY_SPEED_BOT_LIMIT = 3;
    int ENEMY_SPEED_TOP_LIMIT = 4;
    float ENEMY_START_TIME_BOT_LIMIT = 2;
    float ENEMY_START_TIME_TOP_LIMIT = 5;
}
