package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.GameObject;
import com.mygdx.game.Launcher;
import com.mygdx.game.resources.ButtonsRectangles;
import com.mygdx.game.resources.Configuration;
import com.mygdx.game.tools.EnemyCarsSystem;
import com.mygdx.game.resources.Resources;
import com.mygdx.game.tools.Control;
import com.mygdx.game.tools.RainSystem;
import com.mygdx.game.tools.SavedData;
import com.mygdx.game.tools.Tools;
import com.mygdx.game.Сomplexity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Главный класс для взаимодействия всех составляющих компонентов игры
 *
 * @author Волбенко А.К, Макаров Д.А. 17ИТ18
 */
public class MyGdxGame implements Screen, Resources, Configuration, ButtonsRectangles {
    private final Launcher game;
    private GameObject player;
    private final EnemyCarsSystem enemyCarsSystem;
    private final RainSystem rainSystem;
    private final Tools tools;

    private final OrthographicCamera camera;

    private final Control control = new Control();

    private int backgroundY = 0;

    private int introPos = 0;
    private int outroPos = RESOLUTION_WIDTH_MAIN;

    private final Сomplexity complexity = new Сomplexity();

    private final SavedData savedData = new SavedData();

    private final int numCar;

    private boolean stopStatus = false;
    private boolean deadStatus = false;
    private boolean restartStatus = false;
    private boolean exitStatus = false;
    private boolean hitBoxViewStatus = false;

    MyGdxGame(final Launcher gam, int numberCar) {
        this.game = gam;
        player = new GameObject(CARS_TEXTURES[savedData.pref.getInteger("numberCar")],
                PLAYER_X_POS, PLAYER_Y_POS,
                HIT_BOX_CAR_WIDTH, HIT_BOX_CAR_HEIGHT,
                HIT_BOX_CAR_X_POS_SHIFT, 0,
                ANIM_CAR_FRAME_COUNT, ANIM_CAR_CYCLE_TIME);
        numCar = numberCar;
        enemyCarsSystem = new EnemyCarsSystem();
        rainSystem = new RainSystem();
        tools = new Tools();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, RESOLUTION_WIDTH_MAIN, RESOLUTION_HEIGHT_MAIN);
        gam.font.getData().setScale(2, 2);
        //game.shapeRenderer.setProjectionMatrix(camera.combined);
    }

    /**
     * Рендер
     *
     * @param delta - время работы одного тика рендера
     */
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.sb.setProjectionMatrix(camera.combined);

        game.sb.begin();
        //Отрисовка трека
        game.sb.draw(BACKGROUND_MAIN, 0, backgroundY);
        //Отрисовка нижнего слоя финиша
        game.sb.draw(BACKGROUND_FINISH, 0, complexity.getFinishY());
        drawPuddles(rainSystem.getPuddles());
        //Отрисовка машин
        game.sb.draw(player.getTexture(), player.getX(), player.getY(), 30, 75, CAR_WIDTH, CAR_HEIGHT, 1, 1, control.getTurnNumber(player.getX(), LIMIT_LEFT_X_POS, LIMIT_RIGHT_X_POS, CAR_WIDTH));
        enemyCarsDraw(enemyCarsSystem.getEnemyCars());
        //Отрисовка верхнего слоя фишина
        game.sb.draw(BACKGROUND_FINISH_UP_LAYER, 0, complexity.getFinishY());
        //Отрисовка элементов интерфейса
        game.sb.draw(DIFFICULTY_LAP_TEXTURE, 5, 750);
        game.sb.draw(BUTTON_PAUSE, 345, 740);
        game.font.draw(game.sb, " " + complexity.getViewDifficultyLevel(), 55, 786);

        if (rainSystem.getRainLive())
            game.sb.draw(rainSystem.getRainFrame(), 0, 0);

        //Отрисовка картинки стика
        stickDraw(stopStatus);
        drawButtons(stopStatus, deadStatus);
        game.sb.draw(INTRO, introPos, 0);
        game.sb.draw(OUTRO, outroPos, 0);
        game.sb.end();
        //Основные механики и остановка игры
        if (!stopStatus) {
            setBackgroundY();
            update(delta * complexity.getGameDifficultyLevel());
            control.main();
            if (introPos <= INTRO.getWidth() * -1)
                mainSystems(delta * complexity.getGameDifficultyLevel(), complexity.getGameDifficultyLevel());
        }
        introPos = tools.setTransitionPos(introPos, INTRO.getWidth(), 0, INTRO_OUTRO_SPEED);
        editGameScreen();
        checkGameOver();
        checkButtonsPress();
        hitBoxView(hitBoxViewStatus);
    }

    /**
     * Метод обновления данных других методов
     *
     * @param dt - delta time
     */
    private void update(float dt) {
        player.updateAnimation(dt * control.getSpeedAnimation(), true);
        player.setPos(control.playerXPosLimitation(player.getX() + control.getSpeedX(), LIMIT_LEFT_X_POS, LIMIT_RIGHT_X_POS, CAR_WIDTH),
                control.playerYPosLimitation(player.getY() + control.getSpeedY(), LIMIT_TOP_Y_POS, LIMIT_BOTTOM_Y_POS, CAR_HEIGHT));
    }

    /**
     * Основные игроквые системы в виде сложности и вражеских машин
     *
     * @param dt  - delta time
     * @param dif - difficulty
     */
    private void mainSystems(float dt, float dif) {
        complexity.update(dt, player.getHitBox());
        enemyCarsSystem.main(dt, dif);
        rainSystem.update(dt, dif);
        rainSliding();
    }

    /**
     * Скольжение машин при попадании на лужу
     */
    private void rainSliding() {
        player = rainSystem.sliding(player);
        enemyCarsSystem.setEnemyCars(rainSystem.sliding(enemyCarsSystem.getEnemyCars()));
    }

    /**
     * Изменение окна игры
     */
    private void editGameScreen() {
        if (exitStatus || restartStatus)
            outroPos = tools.setTransitionPos(outroPos, OUTRO.getWidth(), RESOLUTION_WIDTH_MAIN, INTRO_OUTRO_SPEED);
        if (outroPos <= RESOLUTION_WIDTH_MAIN - OUTRO.getWidth()) {
            if (exitStatus) game.setScreen(new MenuScreen(game));
            if (restartStatus) game.setScreen(new MyGdxGame(game, numCar));
            saveRecord(complexity.getViewDifficultyLevel());
        }
    }

    /**
     * Сохранение рекорда игрока
     *
     * @param lap - круг
     */
    private void saveRecord(int lap) {
        if (lap > savedData.pref.getInteger("record")) {
            savedData.addIntPreferences("record", lap);
        }
    }

    /**
     * Отрисовка кнопок интерфейса
     *
     * @param stopStatus - статус паузы
     * @param deadStatus - статус окончания игры
     */
    private void drawButtons(boolean stopStatus, boolean deadStatus) {
        if (stopStatus) {
            if (!deadStatus) {
                game.sb.draw(BACKGROUND_PAUSE, 0, 290);
                game.sb.draw(BUTTON_RESUME, 180, 320);
            } else game.sb.draw(BACKGROUND_PAUSE, 0, 345, 400, 160);
            game.sb.draw(BUTTON_EXIT, 130, 400);
            game.sb.draw(BUTTON_RESTART, 230, 400);
        }
    }

    private void drawPuddles(ArrayList<GameObject> puddles) {
        if (puddles.size() != 0)
            for (GameObject puddle : puddles) {
                game.sb.draw(puddle.getTexture(), puddle.getX(), puddle.getY());
            }
    }

    /**
     * Проверка на поражение
     */
    private void checkGameOver() {
        if (tools.checkTouch(player.getHitBox(), enemyCarsSystem.getEnemyCars())) {
            this.stopStatus = this.deadStatus = true;
        }
    }

    /**
     * Отрисовка вражеских машин
     *
     * @param enemyCars - массив с вражескими машинами
     */
    private void enemyCarsDraw(HashMap<GameObject, Integer> enemyCars) {
        for (Map.Entry<GameObject, Integer> enemyCar : enemyCars.entrySet()) {
            game.sb.draw(enemyCar.getKey().getTexture(), enemyCar.getKey().getX(), enemyCar.getKey().getY());
        }
    }

    /**
     * Отрисовывает фон
     */
    private void setBackgroundY() {
        if (backgroundY <= RESOLUTION_HEIGHT_MAIN * -1) backgroundY = 0;
        backgroundY -= BACKGROUND_SPEED * complexity.getGameDifficultyLevel();
    }

    /**
     * Отрисовывает круг стика
     *
     * @param stopStatus - состояние игры
     */
    private void stickDraw(boolean stopStatus) {
        if (control.getTouch() && !stopStatus)
            game.sb.draw(STICK_TEXTURE, control.getTouchPositionX() - 100, control.getTouchPositionY() - 100);
    }

    /**
     * Метод отображения для хитбоксов
     *
     * @param hitBoxView - переменная для переключения отображения
     */
    private void hitBoxView(boolean hitBoxView) {
        if (hitBoxView) {
            game.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            game.shapeRenderer.setColor(1, 1, 0, 1);
            game.shapeRenderer.rect(player.getHitBox().x, player.getHitBox().y, player.getHitBox().width, player.getHitBox().height);
            game.shapeRenderer.end();
        }
    }

    /**
     * Метод запуска работы кнопок
     */
    private void checkButtonsPress() {
        if (!restartStatus && !exitStatus) {
            Rectangle userTouch = tools.userTouchRectangle(Gdx.input.getX(), Gdx.input.getY(), X_SCALE_MAIN, Y_SCALE_MAIN, RESOLUTION_HEIGHT_MAIN);
            //Пауза
            if (userTouch.overlaps(BUTTON_GAME_PAUSE)) stopStatus = true;
            //Выход
            if (userTouch.overlaps(BUTTON_GAME_EXIT) && stopStatus) exitStatus = true;
            //Рестарт
            if (userTouch.overlaps(BUTTON_GAME_RESTART) && stopStatus) restartStatus = true;
            //Продолжить
            if (userTouch.overlaps(BUTTON_GAME_RESUME) && stopStatus && !deadStatus)
                stopStatus = false;
        }
    }


    @Override
    public void show() {

    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        STICK_TEXTURE.dispose();
        BACKGROUND_FINISH.dispose();
        BACKGROUND_FINISH_UP_LAYER.dispose();
        BACKGROUND_MAIN.dispose();
    }
}
