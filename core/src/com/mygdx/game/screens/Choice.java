package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.Launcher;
import com.mygdx.game.resources.ButtonsRectangles;
import com.mygdx.game.resources.Configuration;
import com.mygdx.game.resources.Resources;
import com.mygdx.game.tools.SavedData;
import com.mygdx.game.tools.Tools;

/**
 * Класс отвечающий за окно выбора машины игрока
 *
 * @author Макаров Дмитрий
 */
public class Choice implements Screen, Resources, Configuration, ButtonsRectangles {

    private final Launcher game;
    private OrthographicCamera camera;
    private boolean downBtn;
    private int numberCar;
    private int introPos = 0;
    private int outroPos = 250;
    private Tools tools;

    private int resizeIntroOutroWidth;
    private int resizeIntroOutroHeight;
    private int resizeIntroOutroSpeed;

    private SavedData savedData = new SavedData();

    Choice(Launcher gam) {
        tools = new Tools();
        game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, RESOLUTION_WIDTH_CHOICE, RESOLUTION_HEIGHT_CHOICE);

        resizeIntroOutroWidth = tools.resizeNumberByResolution(INTRO.getWidth(), RESOLUTION_WIDTH_MAIN, RESOLUTION_WIDTH_CHOICE);
        resizeIntroOutroHeight = tools.resizeNumberByResolution(INTRO.getHeight(), RESOLUTION_HEIGHT_MAIN, RESOLUTION_HEIGHT_CHOICE);
        resizeIntroOutroSpeed = tools.resizeNumberByResolution(INTRO_OUTRO_SPEED, RESOLUTION_WIDTH_MAIN, RESOLUTION_WIDTH_CHOICE);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.sb.setProjectionMatrix(camera.combined);
        game.sb.begin();
        game.sb.draw(BACKGROUND_CHOICE, 0, 0);
        drawButtons();
        game.sb.draw(INTRO, introPos, 0, resizeIntroOutroWidth, resizeIntroOutroHeight);
        game.sb.draw(OUTRO, outroPos, 0, resizeIntroOutroWidth, resizeIntroOutroHeight);
        game.sb.end();
        introPos = tools.setTransitionPos(introPos, resizeIntroOutroWidth, 0,resizeIntroOutroSpeed);
        if (introPos <= resizeIntroOutroWidth * -1 && !downBtn) {
            checkPress();
        }
        buttonPress();
    }

    private void buttonPress() {
        if (downBtn) {
            outroPos = tools.setTransitionPos(outroPos, resizeIntroOutroWidth, RESOLUTION_WIDTH_CHOICE, resizeIntroOutroSpeed);
            if (outroPos <= RESOLUTION_WIDTH_CHOICE - resizeIntroOutroWidth) {
                game.setScreen(new MyGdxGame(game, numberCar));
                dispose();
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    /**
     * Отрисовка кнопок выбора машины
     */
    private void drawButtons() {
        game.sb.draw(CARS_CHOICE[0], CHOICE_FIRST_CAR.getX(), CHOICE_FIRST_CAR.getY());
        game.sb.draw(CARS_CHOICE[1], CHOICE_SECOND_CAR.getX(), CHOICE_SECOND_CAR.getY());
        game.sb.draw(CARS_CHOICE[2], CHOICE_THIRD_CAR.getX(), CHOICE_THIRD_CAR.getY());
        game.sb.draw(CARS_CHOICE[3], CHOICE_FOURTH_CAR.getX(), CHOICE_FOURTH_CAR.getY());
        game.sb.draw(CARS_CHOICE[4], CHOICE_FIFTH_CAR.getX(), CHOICE_FIFTH_CAR.getY());
        game.sb.draw(CARS_CHOICE[5], CHOICE_SIXTH_CAR.getX(), CHOICE_SIXTH_CAR.getY());
    }

    /**
     * Проверка нажатия на кнопку
     */
    private void checkPress() {
        Rectangle userTouch = new Rectangle((Gdx.input.getX() * X_SCALE_CHOICE), (RESOLUTION_HEIGHT_CHOICE - Gdx.input.getY() * Y_SCALE_CHOICE), 3, 3);
        if (Gdx.input.isTouched() && userTouch.overlaps(CHOICE_FIRST_CAR)) {
            setChoice(0);
        }
        if (Gdx.input.isTouched() && userTouch.overlaps(CHOICE_SECOND_CAR)) {
            setChoice(1);
        }
        if (Gdx.input.isTouched() && userTouch.overlaps(CHOICE_THIRD_CAR)) {
            setChoice(2);
        }
        if (Gdx.input.isTouched() && userTouch.overlaps(CHOICE_FOURTH_CAR)) {
            setChoice(3);
        }
        if (Gdx.input.isTouched() && userTouch.overlaps(CHOICE_FIFTH_CAR)) {
            setChoice(4);
        }
        if (Gdx.input.isTouched() && userTouch.overlaps(CHOICE_SIXTH_CAR)) {
            setChoice(5);
        }
    }

    /**
     * Запись выбора машины
     *
     * @param numCar - номер машины в массиве тектсур
     */
    private void setChoice(int numCar) {
        savedData.addIntPreferences("numberCar", numCar);
        downBtn = true;
    }
}
