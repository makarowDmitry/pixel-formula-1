package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.Launcher;
import com.mygdx.game.resources.ButtonsRectangles;
import com.mygdx.game.resources.Configuration;
import com.mygdx.game.resources.Resources;
import com.mygdx.game.tools.SavedData;
import com.mygdx.game.tools.Animation;
import com.mygdx.game.tools.Tools;

/**
 * Класс отвечающий за окно главного меню
 *
 * @author Макаров Дмитрий
 */
public class MenuScreen implements Screen, Resources, Configuration, ButtonsRectangles {
    private final Launcher game;
    private Tools tools;

    private Animation animCar1;
    private Animation animCar2;

    private int backgroundY = 0;
    private int firstCarY = -14000;
    private int secondCarY = -28000;
    private int introPos = 0;
    private int outroPos = 400;

    private SavedData savedData = new SavedData();

    private OrthographicCamera camera;
    private boolean startButton = false;
    private boolean choiceButton = false;

    public MenuScreen(Launcher gam) {
        game = gam;

        tools = new Tools();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, RESOLUTION_WIDTH_MAIN, RESOLUTION_HEIGHT_MAIN);

        animCar1 = new Animation((CARS_TEXTURES[4]), 5, 0.5f);
        animCar2 = new Animation((CARS_TEXTURES[5]), 5, 0.5f);
        game.font.getData().setScale(5, 5);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        update(Gdx.graphics.getDeltaTime());

        game.sb.setProjectionMatrix(camera.combined);
        game.sb.begin();
        background();
        game.sb.draw(BUTTON_START, BUTTON_MENU_START.getX(), BUTTON_MENU_START.getY());
        game.sb.draw(BUTTON_CHOICE, BUTTON_MENU_CHOICE.getX(), BUTTON_MENU_CHOICE.getY());
        game.sb.draw(MENU_BEST_LAP, 60, 505, 200, 100);
        game.font.draw(game.sb, "" + savedData.pref.getInteger("record"), 275, 585);//Рекорд
        game.sb.draw(INTRO, introPos, 0);
        game.sb.draw(OUTRO, outroPos, 0);
        game.sb.end();
        introPos = tools.setTransitionPos(introPos, INTRO.getWidth(),0, INTRO_OUTRO_SPEED);
        buttonPress();
        if (introPos <= INTRO.getWidth() * -1 && !startButton && !choiceButton) touchButton();
    }

    /**
     * Метод отвечающий за нажатие на кнопки
     */
    private void touchButton() {
        Rectangle userTouch = tools.userTouchRectangle(Gdx.input.getX(), Gdx.input.getY(), X_SCALE_MAIN, Y_SCALE_MAIN, RESOLUTION_HEIGHT_MAIN);
        if (userTouch.overlaps(BUTTON_MENU_START) && Gdx.input.isTouched()) {
            startButton = true;
        }
        if (userTouch.overlaps(BUTTON_MENU_CHOICE) && Gdx.input.isTouched()) {
            choiceButton = true;
        }
    }

    private void buttonPress() {
        if (startButton || choiceButton) {
            outroPos = tools.setTransitionPos(outroPos, OUTRO.getWidth(), RESOLUTION_WIDTH_MAIN, INTRO_OUTRO_SPEED);
            if (startButton && outroPos <= RESOLUTION_WIDTH_MAIN - OUTRO.getWidth())
                setGameScreen();
            if (choiceButton && outroPos <= RESOLUTION_WIDTH_MAIN - OUTRO.getWidth())
                setChoiceScreen();
        }
    }

    private void setGameScreen() {
        game.setScreen(new MyGdxGame(game, 1));
        dispose();
    }

    private void setChoiceScreen() {
        game.setScreen(new Choice(game));
        dispose();
    }

    /**
     * Обновление анимации машин заднего фона в главном меню
     *
     * @param dt - delta time
     */
    private void update(float dt) {
        animCar1.update(dt, true);
        animCar2.update(dt, true);
    }

    /**
     * Метод отвечающий за задний фон
     */
    private void background() {
        if (backgroundY <= -800) backgroundY = 0;
        game.sb.draw(BACKGROUND_MAIN, 0, backgroundY);

        game.sb.draw(animCar1.getFrame(), 230, firstCarY);
        game.sb.draw(animCar2.getFrame(), 80, secondCarY);

        backgroundY -= 1;

        firstCarY += 16;
        secondCarY += 28;

        if (firstCarY > 30000) firstCarY = -14000;
        if (secondCarY > 100000) secondCarY = -28000;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
