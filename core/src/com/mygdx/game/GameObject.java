package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.tools.Animation;
import com.mygdx.game.resources.Configuration;

/**
 * Класс CarAnimation является классом машины игрока
 *
 * @author Волбенко А.К 17ИТ18
 */
public class GameObject implements Configuration {

    private Animation animation;
    private Rectangle hitBox;

    private int x;
    private int y;
    private int hitBoxXPosShift;
    private int hitBoxYPosShift;

    /**
     * @param objectTexture - текстура объекта
     * @param x             - положение х
     * @param y             - положение y
     */
    public GameObject(Texture objectTexture, int x, int y, int hitBoxWidth, int hitBoxHeight, int hitBoxXPosShift, int hitBoxYPosShift, int frameCount, float cycleTime) {
        this.x = x;
        this.y = y;

        animation = new Animation(objectTexture, frameCount, cycleTime);
        this.hitBoxXPosShift = hitBoxXPosShift;
        this.hitBoxYPosShift = hitBoxYPosShift;
        hitBox = new Rectangle(x + hitBoxXPosShift, y + hitBoxYPosShift, hitBoxWidth, hitBoxHeight);
    }

    public TextureRegion getTexture() {
        return animation.getFrame();
    }

    public Rectangle getHitBox() {
        return hitBox;
    }

    /**
     * обновление положения и обновление анимации
     *
     * @param dt - время одного тика render
     */
    public void updateAnimation(float dt, boolean cycle) {
        animation.update(dt, cycle);
    }

    /**
     * Установка позиции машины
     *
     * @param x - позиция x
     * @param y - позиция y
     */
    public void setPos(int x, int y) {
        this.x = x;
        this.y = y;
        setHitBox(x, y);
    }

    /**
     * Установка позиции хитбокса
     *
     * @param x - позиция x
     * @param y - позиция y
     */
    private void setHitBox(int x, int y) {
        hitBox.setX(x + hitBoxXPosShift);
        hitBox.setY(y + hitBoxYPosShift);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
