package com.mygdx.game;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.resources.Configuration;


/**
 * Класс Difficulty - это класс отвечаюшщий за изменение сложности игры
 *
 * @author Волбенко А.К 17ИТ18
 */
public class Сomplexity implements Configuration {

    private float time = 0;
    private float gameDifficultyLevel = 1;
    private float systemDifficultyLevel = 1;

    private int viewDifLvlDraw = 1;
    private int viewDifLvlSystem = 1;


    private boolean changeDifficultyLvl = false;

    private int finishY = RESOLUTION_HEIGHT_MAIN;

    /**
     * Метод для постоянного поступления tickTime и хитбокса машины игрока
     *
     * @param tickTime - время работы тика render
     * @param player   - хитбокс машины игрока
     */
    public void update(float tickTime, Rectangle player) {
        increaseDifficulty(tickTime);
        finishPositionCalculation();
        difficultyUp(player);
    }

    /**
     * Измененеие сложности в соответствии со временем
     *
     * @param tickTime - время работы тика render
     */
    private void increaseDifficulty(float tickTime) {
        if (!changeDifficultyLvl) time += tickTime;
        if (time >= LAP_TIME) {
            changeDifficultyLvl = true;
            systemDifficultyLevel += 0.1f;
            viewDifLvlSystem += 1;
            time = 0;
        }
    }

    /**
     * Метод изменения положения финиша
     */
    private void finishPositionCalculation() {
        if (changeDifficultyLvl) {
            finishY -= 7 * gameDifficultyLevel;
        }

        if (finishY <= -1670) {
            changeDifficultyLvl = false;
            finishY = RESOLUTION_HEIGHT_MAIN;
        }
    }

    /**
     * Измененеие сложности для игрока после пересечения финиша
     *
     * @param player - хитбокс игрока
     */
    private void difficultyUp(Rectangle player) {
        Rectangle finishLine = new Rectangle(63, finishY + 1222, 274, 15);
        if (finishLine.overlaps(player) && systemDifficultyLevel > gameDifficultyLevel) {
            gameDifficultyLevel = systemDifficultyLevel;
            viewDifLvlDraw = viewDifLvlSystem;
        }
    }

    public int getFinishY() {
        return finishY;
    }

    public float getGameDifficultyLevel() {
        return gameDifficultyLevel;
    }

    public int getViewDifficultyLevel() {
        return viewDifLvlDraw;
    }
}
